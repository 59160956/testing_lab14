var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var mongoose = require('mongoose')

mongoose.connect('mongodb://admin:pass@localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

var db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connected')
})
var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var helloRouter = require('./routes/hello')
var productsRouter = require('./routes/products')
var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())
app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/hello', helloRouter)
app.use('/products', productsRouter)
module.exports = app
