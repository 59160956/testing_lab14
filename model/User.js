var mongoose = require('mongoose')
var Schema = mongoose.Schema
var userSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  gender: {
    type: String,
    enum: ['M', 'F']
  }
})

module.exports = mongoose.model('User', userSchema)
