const dbHandler = require('./db-handler')
const User = require('../model/User')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})
afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete = {
  name: 'Max',
  gender: 'M'
}
const userComplete2 = {
  name: 'Max',
  gender: 'F'
}

const userErrorNameEmtry =
{
  name: '',
  gender: 'M'
}
const userErrorName2Alphabets =
{
  name: 'Ma',
  gender: 'M'
}
const userErrorGenderInvalid = {
  name: 'Max',
  gender: 'A'
}

describe('User', () => {
  it('สามารถเพิ่ม user ได้ M', async () => {
    let error = null
    try {
      const user = new User(userComplete)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่ม user ได้ F', async () => {
    let error = null
    try {
      const user = new User(userComplete2)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
})
describe('User', () => {
  it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น ช่องว่าง', async () => {
    let error = null
    try {
      const user = new User(userErrorNameEmtry)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
describe('User', () => {
  it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น 2 ตัว', async () => {
    let error = null
    try {
      const user = new User(userErrorName2Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
describe('User', () => {
  it('ไม่สามารถเพิ่ม user ได้ เพราะ gender ไม่ถูกต้อง A', async () => {
    let error = null
    try {
      const user = new User(userErrorGenderInvalid)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
describe('User', () => {
  it('ไม่สามารถเพิ่ม user ชื่อซ้ำกันได้', async () => {
    let error = null
    try {
      const user1 = new User(userComplete)
      await user1.save()
      const user2 = new User(userComplete)
      await user2.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
